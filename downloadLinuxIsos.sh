#!/bin/bash

if [ $# -eq 0 ]
then
   dir="."
else
   dir=$1
fi

#Fedora 28
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-Cinnamon-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-KDE-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-LXDE-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-LXQt-Live-x86_64-28-1.1.iso 
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-MATE_Compiz-Live-x86_64-28-1.1.iso 
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-SoaS-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Spins/x86_64/iso/Fedora-Xfce-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Server/x86_64/iso/Fedora-Server-dvd-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Server/x86_64/iso/Fedora-Server-netinst-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Container/x86_64/images/Fedora-Container-Base-28-1.1.x86_64.tar.xz
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Container/x86_64/images/Fedora-Container-Minimal-Base-28-1.1.x86_64.tar.xz
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.raw.xz
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-Vagrant-28-1.1.x86_64.vagrant-libvirt.box
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-Vagrant-28-1.1.x86_64.vagrant-virtualbox.box
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/AtomicWorkstation/x86_64/iso/Fedora-AtomicWorkstation-ostree-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/AtomicHost/x86_64/images/Fedora-AtomicHost-28-1.1.x86_64.qcow2
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/AtomicHost/x86_64/images/Fedora-AtomicHost-28-1.1.x86_64.raw.xz
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/AtomicHost/x86_64/images/Fedora-AtomicHost-Vagrant-28-1.1.x86_64.vagrant-libvirt.box
wget -P $dir -c --no-check-certificate http://fedora.gtdinternet.com/releases/28/AtomicHost/x86_64/images/Fedora-AtomicHost-Vagrant-28-1.1.x86_64.vagrant-virtualbox.box
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Astronomy_KDE-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Design_suite-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Games-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Jam_KDE-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Python-Classroom-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Robotics-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Scientific_KDE-Live-x86_64-28-1.1.iso
wget -P $dir -c --no-check-certificate http://fedora-alt.c3sl.ufpr.br/releases/28/Labs/x86_64/iso/Fedora-Security-Live-x86_64-28-1.1.iso

#Ubuntu 16.04.X LTS
wget -P $dir -c --no-check-certificate http://releases.ubuntu.com/18.04/ubuntu-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://releases.ubuntu.com/18.04/ubuntu-18.04-live-server-amd64.iso
wget -O ubuntu-16.04.4-core-amd64.iso -P $dir -c --no-check-certificate http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso
wget -O ubuntu-16.04.4-core-i386.iso -P $dir -c --no-check-certificate http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-i386/current/images/netboot/mini.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/cdimage.ubuntu.com/cdimage/xubuntu/releases/18.04/release/xubuntu-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/cdimage.ubuntu.com/cdimage/xubuntu/releases/18.04/release/xubuntu-18.04-desktop-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntustudio/releases/bionic/release/ubuntustudio-18.04-dvd-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntustudio/releases/bionic/release/ubuntustudio-18.04-dvd-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntu-mate/releases/18.04/release/ubuntu-mate-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntu-mate/releases/18.04/release/ubuntu-mate-18.04-desktop-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntu-budgie/releases/18.04/release/ubuntu-budgie-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/ubuntu-budgie/releases/18.04/release/ubuntu-budgie-18.04-desktop-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-desktop-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-alternate-i386.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-alternate-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/kubuntu/releases/18.04/release/kubuntu-18.04-desktop-amd64.iso
wget -P $dir -c --no-check-certificate http://cdimage.ubuntu.com/kubuntu/releases/18.04/release/kubuntu-18.04-desktop-i386.iso

#Debian netinst,CD & DVD 9.4.0 (stable)
wget -P $dir -c --no-check-certificate http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.4.0-amd64-netinst.iso
wget -P $dir -c --no-check-certificate http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.4.0-amd64-xfce-CD-1.iso
wget -P $dir -c --no-check-certificate http://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-9.4.0-amd64-DVD-1.iso
wget -P $dir -c --no-check-certificate http://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-9.4.0-amd64-DVD-2.iso
wget -P $dir -c --no-check-certificate http://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-9.4.0-amd64-DVD-3.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/release/current/i386/iso-cd/debian-9.4.0-i386-netinst.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/release/current/i386/iso-cd/debian-9.4.0-i386-xfce-CD-1.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/release/current/i386/iso-dvd/debian-9.4.0-i386-DVD-1.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/release/current/i386/iso-dvd/debian-9.4.0-i386-DVD-2.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/release/current/i386/iso-dvd/debian-9.4.0-i386-DVD-3.iso

#Debian testing
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/debian-testing-amd64-netinst.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/debian-testing-amd64-xfce-CD-1.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-cd/debian-testing-i386-netinst.iso
wget -P $dir -c --no-check-certificate https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-cd/debian-testing-i386-xfce-CD-1.iso

#Linux Mint Sylvia (18.3)
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-cinnamon-32bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-cinnamon-64bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-kde-32bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-kde-64bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-mate-32bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-mate-64bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-xfce-32bit.iso
wget -P $dir -c --no-check-certificate http://www.mirrorservice.org/sites/www.linuxmint.com/pub/linuxmint.com/stable/18.3/linuxmint-18.3-xfce-64bit.iso

#Linux Mint Debian Edition 2 "Betsy"
wget -P $dir -c --no-check-certificate http://mirrors.kernel.org/linuxmint/debian/lmde-2-201701-mate-32bit.iso
wget -P $dir -c --no-check-certificate http://mirrors.kernel.org/linuxmint/debian/lmde-2-201701-mate-64bit.iso
wget -P $dir -c --no-check-certificate http://mirrors.kernel.org/linuxmint/debian/lmde-2-201701-cinnamon-32bit.iso
wget -P $dir -c --no-check-certificate http://mirrors.kernel.org/linuxmint/debian/lmde-2-201701-cinnamon-64bit.iso

