#!/bin/bash
#   releaseIP.sh
#
#   Copyright 2016 Eduard Lucena <x3mboy@WorkLaptop>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.


ip=$(ifconfig -a | awk '/^wlp6s0/,/^$/' | awk '/inet/ { print $2 }' | cut -d: -f2)
echo -e "Your Ip address for wlp6s0 is: \e[1;49;34m$ip\e[0m"
echo -e "Pinging \e[1;49;34mG\e[0m\e[1;49;31mo\e[0m\e[1;49;33mo\e[0m\e[1;49;34mg\e[0m\e[1;49;32ml\e[0m\e[1;49;31me\e[0m"
if  ! ping -q -c 1 google.com > /dev/null 2>&1 ; then
   sudo dhclient -r wlp6s0
   sudo ifconfig wlp6s0 up
   sudo dhclient wlp6s0
   ip=$(ifconfig -a | awk '/^wlp6s0/,/^$/' | awk '/inet/ { print $2 }' | cut -d: -f2)
   echo -e "Ip address for wlp6s0 after release is: \e[1;49;34m$ip\e[0m"
   echo -e "Pinging \e[1;49;34mG\e[0m\e[1;49;31mo\e[0m\e[1;49;33mo\e[0m\e[1;49;34mg\e[0m\e[1;49;32ml\e[0m\e[1;49;31me\e[0m"
   if  ! ping -q -c 1 google.com > /dev/null 2>&1 ; then
      echo -e "Something went wrong. Try Again!"
   else
      echo -e "Everything is Ok!"
   fi
else
   echo -e "Everything is Ok!"
fi
