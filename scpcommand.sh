#!/bin/bash

#   scpcommand.sh
#
#   Copyright 2011 Eduard Lucena <x3mboy@tifa>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.


#   Script diseñado para copiar archivos desde un servidor ssh a otro y viceversa
#   Recibe dos parámetros por la línea de comandos:
#   1.- El primer parámetro es el archivo a copiar
#   2.- Un número entero con dos posibles valores:
#       0 - Copia hacia el servidor desde el ejecutante del script
#       1 - Copia desde el servidor hacia el ejecutante del script
#   3.- La direccion IP del Host
#   4.- El usuario en el servidor receptor
#   5.- El password del usuario en el servidor receptor
#   6.- Ruta donde se guardara el archivo copiado
#
#   NdA: El servidor ejecutante no tiene que ser un servidor pero para poder enviar
#   archivos desde el, debe estar instalado el servicio ssh, especificamente sshd
#   NdA: Se debe instalar el paquete expect para ejecutar el script.


distro=$(lsb_release -si)

if [[ $distro == "Fedora" ]]; then
   prueba=$(rpm -qa expect)
fi
if [[ $distro == "Ubuntu" || $distro == "LinuxMint" || $distro == "Debian" ]]; then
   prueba=$(dpkg --get-selections | grep expect)
fi
if [[ $prueba == "" ]]; then
   echo "No se encuentra instalado el paquete expect, necesario para la ejecución del script"
fi
FILE=$1
ORDER=$2
HOST=$3
USER=$4
PASS=$5
RUTA=$6
PARSEFILE=$(echo $FILE | cut -c 1)

if [ $ORDER -eq 0 ]; then
   VAR=$(expect -c "
   spawn scp -o StrictHostKeyChecking=no $FILE $USER@$HOST:$RUTA
   match_max 100000
   expect \"?assword:*\"
   send -- \"$PASS\r\"
   send -- \"\r\"
   expect eof
   ")
   echo "==============="
   echo "$VAR"
else
   if [ $ORDER -eq 1 ]; then
#      if [ $PARSEFILE -eq "/" ]; then
#         RUTA=$FILE
#      else
#         RUTA=$RUTA$FILE
#      fi
      VAR=$(expect -c "
      spawn scp -o StrictHostKeyChecking=no $USER@$HOST:$RUTA ./
      match_max 100000
      expect \"?assword:*\"
      send -- \"$PASS\r\"
      send -- \"\r\"
      expect eof
      ")
      echo "==============="
      echo "$VAR"
   else
      echo "Parametros incorrectos\n"
   fi
fi

