#!/bin/bash
 
letter1=( a b c d f g h i j k l m n o p q r s t u v w x y z )
letter2=( a b c d f g h i j k l m n o p q r s t u v w x y z )
 
for i in "${letter1[@]}"
do
    for j in "${letter2[@]}"
    do
        sudo dnf clean all
        sudo dnf -y reinstall "$i$j*"
    done
done
