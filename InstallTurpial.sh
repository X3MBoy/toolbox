#!/bin/bash
USER_HOME=$(eval echo ~${SUDO_USER})
if [ `whoami` != root ]; then
    echo Please run this script using sudo
    exit
fi
 
distro=$(lsb_release -si)
echo $distro
 
echo Revisando dependencias
dependenciasFaltantes=()
if [[ $distro == "Fedora" ]]; then
   prueba=$(rpm -qa git)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[0]="git"
   fi
   prueba=$(rpm -qa python-jinja2)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[1]="python-jinja2"
   fi
   prueba=$(rpm -qa notify-python)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[2]="notify-python"
   fi
   prueba=$(rpm -qa python-babel)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[3]="python-babel"
   fi
   prueba=$(rpm -qa PyQt4)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[4]="PyQt4"
   fi
   prueba=$(rpm -qa python-setuptools)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[5]="python-setuptools"
   fi
   prueba=$(rpm -qa gstreamer-python)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[6]="gstreamer-python"
   fi
   cantidad=${#dependenciasFaltantes[@]}
   if [[ $cantidad == 0 ]]; then
      echo No hay dependencias faltantes
   else
      echo Hay dependencias faltantes
      echo ${dependenciasFaltantes[@]}
      echo 'Desea Instalarlas? (s/n)'
      read respuesta
      if [[ $respuesta == "s" || $respuesta == "S" ]]; then
         yum -y install ${dependenciasFaltantes[@]}
      else
         echo Las dependencias son necesarias, adiós.
         exit
      fi
   fi
fi
if [[ $distro == "Ubuntu" || $distro == "LinuxMint" ]]; then
   prueba=$(dpkg --get-selections | grep git)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[0]="git"
   fi
   prueba=$(dpkg --get-selections | grep python-jinja2)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[1]="python-jinja2"
   fi
   prueba=$(dpkg --get-selections | grep python)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[2]="python"
   fi
   prueba=$(dpkg --get-selections | grep python-babel)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[3]="python-babel"
   fi
   prueba=$(dpkg --get-selections | grep python-qt4)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[4]="python-qt4"
   fi
   prueba=$(dpkg --get-selections | grep python-setuptools)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[5]="python-setuptools"
   fi
   prueba=$(dpkg --get-selections | grep python-gst0.10)
   if [[ $prueba == "" ]]; then
     dependenciasFaltantes[6]="python-gst0.10"
   fi
   cantidad=${#dependenciasFaltantes[@]}
   if [[ $cantidad == 0 ]]; then
      echo No hay dependencias faltantes
   else
      echo Hay dependencias faltantes
      echo ${dependenciasFaltantes[@]}
      echo 'Desea Instalarlas? (s/n)'
      read respuesta
      if [[ $respuesta == "s" || $respuesta == "S" ]]; then
         apt-get -y install ${dependenciasFaltantes[@]}
      else 
         echo Las dependencias son necesarias, adiós.
         exit
      fi
   fi
fi
if [[ $distro != "Fedora" && $distro != "Ubuntu" && $distro != "LinuxMint" ]]; then
   echo 'Script sólo para Fedora, Ubuntu y Linux Mint (por los momentos), contacte a Eduard Lucena (eduardlucena@gmail.com) para colaborar en la migración a otras Distros.'
   exit
fi
 
cd $USER_HOME
git clone git://github.com/satanas/libturpial.git libturpial
cd libturpial
git checkout development
sudo python setup.py develop
 
cd $USER_HOME
git clone git://github.com/satanas/Turpial.git turpial
cd turpial
git checkout development
sudo python setup.py develop
 
echo Instalación exitosa
