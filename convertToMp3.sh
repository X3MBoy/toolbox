#!/bin/sh

#   convertToMp3.sh
#
#   Copyright 2017 Eduard Lucena <x3mboy@inewclient>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

#   Script designed to convert m4a files to mp3.
#
#   The script receives a the path where the files are.

cd "$1"
echo $PWD
for i in *.m4a;
    do name=`echo $i | cut -d'.' -f1`;
        echo $name;
        ffmpeg -i "$i" "$name.mp3";
    done
