This is a little repository with some tools that I use in a daily basis. It will be growing with the time, while I will be adding and updating the tools.

Right now there are some bash scripts, also some python tools and my configuration files, like my <strong>vimrc</strong> and <strong>zshrc</strong>.

# Using config files

## vimrc

For the vimrc file you can do 2 things:
- Copy the file into ~/.vimrc

<code>cp vimrc ~/.vimrc</code>

- Use the -u directive with vim

<code>vim -u vimrc FileToOpen</code>

## zshrc

This zshrc is to use it with [oh my zsh!](https://ohmyz.sh/) and the instruction is just to copy it into .zshrc:

<code>cp zshrc ~/.zshrc</code>

This one has my preferences:

- Plugins
    - git
    - history
    - python
    - colorize
    - dnf
- Theme
    - [ys](https://github.com/robbyrussell/oh-my-zsh/wiki/External-themes#nodeys)
- Daily update of [oh my zsh!](https://ohmyz.sh/)

## Touchpad

For i3wm, there is a right way to accomplish the tap-to-click that consist in declare the touchpad to the xserver.

The code is in the file <code>90-touchpad.conf</code> and you just need to copy that to the right location:

<code>sudo mkdir -p /etc/X11/xorg.conf.d
sudo cp 90-touchpad.conf /etc/X11/xorg.conf.d</code>

Take into account that for this to work, you will need to have installed <code>xinput</code>

