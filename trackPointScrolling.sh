#!/bin/sh
#   trackPointScrolling.sh
#
#   Copyright 2016 Eduard Lucena <x3mboy@Penny>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

xinput list | sed -ne 's/^[^ ][^V].*id=\([0-9]*\).*/\1/p' | while read id
do
    echo $id
    case `xinput list-props $id` in
    *"Middle Button Emulation"*)
        xinput set-int-prop $id "Evdev Wheel Emulation" 8 1
        xinput set-int-prop $id "Evdev Wheel Emulation Button" 8 2
        xinput set-int-prop $id "Evdev Wheel Emulation Timeout" 8 200
        xinput set-int-prop $id "Evdev Wheel Emulation Axes" 8 6 7 4 5
        xinput set-int-prop $id "Evdev Middle Button Emulation" 8 1
        ;;
    esac
done
