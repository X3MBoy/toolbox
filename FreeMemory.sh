#!/bin/bash

#   FreeMemory.sh
#
#   Copyright 2011 Eduard Lucena <x3mboy@tifa>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

#   Script designed to release RAM through the file
#   /proc/sys/vm/drop_caches
#
#   The script doesn't receive any parameter but it's intended to run
#   as "root" with "sudo".

#   Script diseñado para liberar la memoria RAM, a través del
#   archivo /proc/sys/vm/drop_caches.
#
#   El script no recibe ningún parámetro, pero debe ser ejecutado en
#   "root" o con "sudo" en un usuarios con privilegios en el sistema

USER_HOME=$(eval echo ~${SUDO_USER})
if [ `whoami` != root ]; then
   echo Please run this script using sudo
   exit
fi

echo "Memory state before"
free -h
echo 
echo
sync;swapoff -a && swapon -a
sync;echo 1 > /proc/sys/vm/drop_caches
sync;echo 2 > /proc/sys/vm/drop_caches
sync;echo 3 > /proc/sys/vm/drop_caches
sync;echo 4 > /proc/sys/vm/drop_caches
sync;echo 1 > /proc/sys/vm/drop_caches
sync;echo 2 > /proc/sys/vm/drop_caches
sync;echo 3 > /proc/sys/vm/drop_caches
sync;echo 4 > /proc/sys/vm/drop_caches
sync
echo "Memory state after"
free -h
