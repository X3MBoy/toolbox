#!/bin/bash

#   fecha.sh
#
#   Copyright 2011 Eduard Lucena <x3mboy@tifa>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.


#   Script diseñado para obtener la fecha del dia anterior sin usar
#   la version date de GNU. Solo porque en algunas versiones UNIX
#   el programa date no recibe el parametro --date=yesterday
#
#   El script no recibe ningún parámetro.

dia=$(date +%d)
diaAyer=$((dia-1))
mesActual=$(date +%m)
mesAnterior=$((mesActual-1))
anno=$(date +%Y)
if [ $diaAyer -eq 0 ]; then
   if [ $mesAnterior -eq 2 ]; then
      if [ $anno % 4 -eq  0 ] && [ $anno % 100 -ne 0 ] || [ $anno % 400 -eq 0 ]; then
         diaAyer=29
      else
         diaAyer=28
   fi
   else
      if [ $mesAnterior -eq  1 ] || [ $mesAnterior -eq 3 ] || [ $mesAnterior -eq 5 ] || [ $mesAnterior -eq 7 ] || [ $mesAnterior -eq 8 ] || [ $mesAnterior -eq 10 ] || [ $mesAnterior -eq 12 ]; then
         diaAyer=31
      else
         diaAyer=30
      fi
   fi
   if [ $diaAyer -lt 10 ]; then
      diaAyer="0""$diaAyer"
   fi
   if [ $mesAnterior -lt 10 ]; then
      fecha="$diaAyer""0""$mesAnterior""$anno"
   else
      fecha="$diaAyer""$mesAnterior""$anno"
   fi
else
   if [ $diaAyer -lt 10 ]; then
      diaAyer="0""$diaAyer"
   fi
   mesAnno=$(date +%m%Y)
   fecha="$diaAyer""$mesAnno"
fi

echo $fecha
