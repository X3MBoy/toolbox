#!/bin/bash
USER_HOME=$(eval echo ~${SUDO_USER})
if [ `whoami` != root ]; then
    echo Please run this script using sudo
    exit
fi

cd $USER_HOME/libturpial
git pull origin master
sudo python setup.py develop
cd $USER_HOME/turpial
git pull origin development
sudo python setup.py develop
