#!/bin/bash

find $HOME -type d -print0 | xargs -0 chmod 0775
find $HOME -type f -print0 | xargs -0 chmod 0664
