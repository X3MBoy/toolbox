#!/bin/bash
#
#   dnfUpgrade.sh
#
#   Copyright 2011 Eduard Lucena <x3mboy@inewserver>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

echo "Running upgrade --refresh"
sudo dnf -y upgrade --refresh
echo "Installing plugin for upgrades"
sudo dnf -y install dnf-plugin-system-upgrade
echo "Downloading upgrade's packages"
sudo dnf -y system-upgrade download --refresh --releasever=`expr $(lsb_release -sr) + 1` --allowerasing --best
if [ $? -eq 0 ]; then
    echo "Your system is ready to reboot and install the upgrade. Are you going to reboot [y/N]?"
    read answer
    case $answer in
        y | Y)
            sudo dnf system-upgrade reboot
	    ;;
        n | N)
            echo "Exiting. To finish the upgrade you need to run the command:"
            echo -ne "\e[1;49;32m[0m sudo dnf system-upgrade reboot"
            exit 1
            ;;
    esac
else
   echo "Something went wrong! Please check the download or the network."
fi
