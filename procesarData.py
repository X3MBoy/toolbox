#!/usr/bin/python

#   procesarData.py
#
#   Copyright 2011 Eduard Lucena <x3mboy@tifa>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.


#  Algunas funciones de fecha y de trabajo con MySQL,
#  sólo son prácticas de algunas cosas que he estado haciendo y
#  las dejo aquí como referencia.

import datetime
import MySQLdb

#  Trabajar con la fecha. Para restar o sumar tiempo a una fecha se aprovecha
#  la funcion timedelta del Objeto datetime. Para este caso restamos un dia
fecha = datetime.date.today() - datetime.timedelta(days=1)
ayer = fecha.strftime("%d%m%Y")
print ayer."\n";

#  Leer desde un archivo de texto e insertar en una BD MySQL.
#  En este caso el archivo es un csv sin cabeceras guardado como "archivo.txt"
db = MySQLdb.connect(host='localhost',user='root',passwd='admin','db')
c = db.cursor()
archivo = open("archivo.txt","r")
arreglo = []
for linea in archivo:
	lineaInsertar = linea.split(",")
#   Aca simplemente se elimino el retorno de carro, la marca de fin de linea o como prefieran llamarlo, en cada una de las lineas
	lineaInsertar[len(lineaInsertar)-1]=lineaInsertar[len(lineaInsertar)-1].replace("\n","")
#   Se inserta cada linea. Es importante conocer la cantidad de campos y el orden de los mismos, pero esto es mas SQL que Python
    c.execute("""insert into tabla(campo1,campo2,campo3,campo4,campo5,campo6,campo7,campo7,campo9,campo10,campo11,campo12,campo13,campo14,campo15) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",lineaInsertar)
	arreglo.append(lineaInsertar)
archivo.close()
#  Hay un caso aqui. La funcion executemany parece tener un limite, aun no se
#  si de tamaño en memoria del arreglo o si de cantidad de registro. Puedo decir que
#  hasta con 13K registros funciono, el siguiente caso que probe fue de 1M de registros
# y fue alli donde fallo.
c.executemany("""insert into tabla(campo1,campo2,campo3,campo4,campo5,campo6,campo7,campo7,campo9,campo10,campo11,campo12,campo13,campo14,campo15) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",arreglo)
c.close()
db.commit()
db.close()
