#!/bin/bash

#   sshcommand.sh
#
#   Copyright 2011 Eduard Lucena <x3mboy@tifa>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.


#   Script para ejecutar comandos por ssh en el MDN
#   Basado en el script de nixCraft:
#   ------------------------------------------------------------------------
#   Copyright (c) 2004 nixCraft project <http://cyberciti.biz/fb/>
#   This script is licensed under GNU GPL version 2.0 or above
#   ------------------------------------------------------------------------
#   This script is part of nixCraft shell script collection (NSSC)
#   Visit http://bash.cyberciti.biz/ for more information.
#   ------------------------------------------------------------------------
#
#   Con las variaciones mostradas en el blog de ubuntulife:
#
#   http://ubuntulife.wordpress.com/2010/08/03/sencillo-script-en-bash-para-ejecutar-un-comando-remoto-mediante-ssh/
#
#   El script recibe 4 parametros:
#
#   1.- El comando a ejecutar
#   2.- La direccion IP del host donde se va a ejecutar el comando
#   3.- Usuario en el servidor donde se ejecutar el comando
#   4.- Password del usuario en el servdor donde se va a ejecuta el comando.
#
#   NdA: Si el comando es complejo, con tuberias ("|" o "pipes") y/o parametros propios, debe enviarse entre
#   comillas dobles "" el comando completo.
#
#   Ejemplo de uso:
#   ./sshcommand "cat datos.txt | wc -l" 172.0.0.1 usuario clave

CMD=$1
HOST=$2
USER=$3
PASS=$4
VAR=$(expect -c "
spawn ssh -o StrictHostKeyChecking=no $USER@$HOST $CMD
match_max 100000
expect \"?assword:*\"
send -- \"$PASS\r\"
send -- \"\r\"
expect eof
")
echo "==============="
echo "$VAR"
