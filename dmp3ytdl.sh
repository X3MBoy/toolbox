#!/bin/bash

#   dmp3ytdl.sh
#
#   Copyright 2017 Eduard Lucena <x3mboy@moealucena>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

#   Script designed to download audio from youtube videos in mp3 format.
#
#   The script receives the URL where the audio will be extracted.


URL=$1
MissingDependencies=()
test=$(rpm -qa youtube-dl)
if [[ $test == "" ]]; then
   MissingDependencies[0]="youtube-dl"
fi
test=$(rpm -qa ffmpeg)
if [[ $test == "" ]]; then
   MissingDependencies[1]="ffmpeg"
fi
amount=${#MissingDependencies[@]}
if [[ $amount != 0 ]]; then
   echo "There are dependencies that needs to be installed"
   echo "Do you want to install it? (Y/N)"
   read answer
   looper=1
   while [ $looper == 1 ]; do
      if [[ $answer == "Y" || $answer == "y" ]]; then
         looper = 0
         dnf -y install ${MissingDependencies[@]}
      else
         if [[ $answer != "N" || $answer != "n" ]]; then
            echo "Please select (Y)es or (N)o"
            read answer
         else
            echo "Dependencies are need it, please install it before using this"
            exit
         fi
      fi
   done
fi
youtube-dl -x --audio-format mp3 $URL

