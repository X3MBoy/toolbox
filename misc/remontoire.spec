Name:           remontoire
Version:        1.4.3
Release:        1%{?dist}
Summary:        Remontoire app to show keybindings

License:        GPL-3.0
URL:            https://github.com/regolith-linux/remontoire/
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  gtk3-devel
BuildRequires:  libgee-devel
BuildRequires:  json-glib-devel
BuildRequires:  cmake
BuildRequires:  meson
BuildRequires:  ninja-build
BuildRequires:  vala
Requires:       gtk3

%description
Remontoire is a small GTK app for presenting keybinding hints in a compact form suitable for tiling window environments. It is intended for use with the i3 window manager but it's also able to display keybindings from any suitably formatted config file.

%prep
%autosetup -n %{name}-%{version}

%build
%meson
%meson_build

%install
%meson_install

%files
%license COPYING
%{_bindir}/remontoire
%{_datadir}/appdata/org.regolith-linux.remontoire.appdata.xml
%{_datadir}/applications/org.regolith-linux.remontoire.desktop
%{_datadir}/glib-2.0/schemas/org.regolith-linux.remontoire.gschema.xml


%changelog
* Tue Feb 04 2025 x3mboy <eduardlucena@gmail.com>
- 
