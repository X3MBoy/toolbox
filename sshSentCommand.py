#!/bin/env python
# -*- coding: utf-8 -*-

#   comandoSSH.py
#
#   Copyright 2016 Eduard Lucena <x3mboy@WorkLaptop>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.

import paramiko
__author__ = "x3mboy"
__date__ = "$05/02/2016 10:55:00 AM$"

ssh_server = 'servidor'
ssh_user = 'usuario'
ssh_pass = 'password'
ssh_port = 22
command = 'ls'

conn = paramiko.Transport((ssh_server, ssh_port))
conn.connect(username=ssh_user, password=ssh_pass)

channel = conn.open_session()
channel.exec_command(command)

salida = channel.makefile('rb', -1).readlines()

if salida:
    print(salida)
else:
    print((channel.makefile_stderr('rb', -1).readlines()))
conn.close()
